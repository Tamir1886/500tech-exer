import React from 'react';

const Grid = ({ columns, data }) => {

  const Columns = () => {
    return (
      <tr>
        {columns.map((item) => {
          return (
            <th key={item.title}>{item.title}</th>
          )
        })}
      </tr>
    )
  }


  const MoviesRows = () => {
    let parserData = data.map((movie, index) => {
      return (
        <tr key={index}>
          {
            columns.map((item, j) => {
              let tempFiled = item.field;
              let movieDetail=movie[tempFiled]
              return (
                item.component ? <item.component key={j} data={movieDetail} /> : <td key={j}>{movieDetail}</td>)
            })
          }
        </tr>


      )
    })
    return parserData;
  }


  return (
    <>
      <table>
        <thead>
          <Columns />
        </thead>
        <tbody>
          <MoviesRows />
        </tbody>
      </table>
    </>
  )
};

export default Grid;